#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <memory.h>
//===================================== Global Variables ==========================================
pthread_t tid[2]; // 2 threads untuk unzip
pid_t child;
char *home = "/Users/rainataputra/Documents/4th Term/Operating System/Shift 3";
//===================================== Kumpulan Fungsi ==========================================

// Thread functions
void * unzip_file (void *arg);
void * create_no_txt (void *arg);
void * unzip_file2 (void *arg);
// Regular functions
void initialize();
void make_dir(char *dir_name);
void zip_wp(char *zip_name, char *password);
void move_to(char *dir_name, char *file_name);
void print_cwd();
void remove_file(char *file);
void *grab_str(void *dir); //decode process goes here

//======================= functions bodies

// inisialisasi
void initialize(){
    char *output[] = {"quote.zip", "music.zip"};
    char *links[] = {
        "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
        "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
    
    // make a folder
    make_dir("quote");
    make_dir("music");

    // download
    for(int i=0; i<2; i++){
        pid_t child = fork();
        if(child == 0){
            char *argv[] = {"wget","-q","--no-check-certificate",links[i],"-O",output[i],NULL};
            execv("/usr/local/bin/wget",argv);
        }
        waitpid(child, NULL, 0);
    }

    sleep(1);
}

// remove file
void remove_file(char *file){
    pid_t id = fork();
    if(id == 0){
        char *argv[] = {"rm",file,NULL};
        execv("/bin/rm", argv);
    }
    waitpid(id,NULL,0);
}

// make dir
void make_dir(char *dir_name){
    pid_t child = fork();
    if(child == 0){
        char *argv[] = {"mkdir","-p",dir_name,NULL};
		execv("/bin/mkdir", argv);
    }
    waitpid(child,NULL,0);
}

// move file
void move_to(char *dir_name, char *file_name){
    pid_t child = fork();
    if(child == 0){
        char *argv[] = {"mv", file_name, dir_name, NULL};
        execv("/bin/mv", argv);
    }
    waitpid(child, NULL, 0);
}

// print cwd
void print_cwd(){
    char t[100];
    printf("%s\n", getcwd(t,100));
}

// copy_move
void copy_move(char *source, char*dest){
    pid_t id = fork();
    if(id == 0){
        char *argv[] = {"cp",source,dest,NULL};
        execv("/bin/cp", argv);
    }
    waitpid(id,NULL,0);
}

// unzip_file
void *unzip_file(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){
        child = fork();
        if(child == 0){
            char *argv[] = {"unzip","-qq","quote.zip","-d","quote",NULL};
            execv("/usr/bin/unzip",argv);
        }
        waitpid(child,NULL,0);
    }
    else if(pthread_equal(id, tid[1])){
        child = fork();
        if(child == 0){
            char *argv[] = {"unzip","-qq","music.zip","-d","music",NULL};
            execv("/usr/bin/unzip",argv);
        }
        waitpid(child,NULL,0);
    }

    return NULL;
}


// grab str quote
void *grab_str(void *dire){
    char *dir;
    dir = (char *)dire;
    //printf("%s\n", dir);
    //chdir(dir);
    char *zipfile,*awal;
    if(strcmp(dir,"quote") == 0){
        awal = "q"; 
    }
    else{
        awal = "m";
    }
    char *txt;
    char *num;
    char *file_name;
    char txt_merge[100];
    char final_txt[100];

    for(int i=1;i<=9;i++){
        txt = (char*)malloc(100); num = (char*)malloc(100); file_name = (char*)malloc(100);
        strcpy(file_name, awal);
        sprintf(num,"%d",i); strcat(file_name,num);
        strcpy(txt_merge,file_name); strcat(txt_merge,"_new.txt");
        strcat(file_name,".txt");

        pid_t id = fork();
        if(id == 0){
            char *argv[] = {"base64", "-d", "-i", file_name, "-o", txt_merge,NULL};
            execv("/usr/bin/base64",argv);
        }
        waitpid(id,NULL,0);
        remove_file(file_name);

        //joining the decoded file
        char *temp = (char*)malloc(100);
        strcpy(final_txt,dir);strcat(final_txt,".txt");
        FILE *file = fopen(final_txt,"a");
        FILE *input = fopen(txt_merge,"r");
        fgets(temp,100,input);
        fputs(temp,file);
        fprintf(file,"\n");
        fclose(file);fclose(input);
        remove_file(txt_merge);


    }
    chdir(home);
    return NULL;
}

// create no txt
void *create_no_txt(void *arg){
    FILE *file = fopen("no.txt", "w");
    fputs("No",file);
    fclose(file);
    return NULL;
}

// unzip file 2
void * unzip_file2(void *arg){
    char *pass = (char*)arg;
    pid_t id = fork();
    if(id == 0){
        char *argv[] = {"unzip","-P",pass, "hasil.zip",NULL};
        execv("/usr/bin/unzip",argv);
    }
    waitpid(id,NULL,0);
    return NULL;
}

int main(){
    initialize();
    print_cwd();
    if(pthread_create(&tid[0],NULL,&unzip_file, NULL)){
        printf("Thread quote error\n");
        exit(EXIT_FAILURE);
    }
    print_cwd();
    // thread 2
    if(pthread_create(&tid[1],NULL,&unzip_file, NULL)){
        printf("Thread music error\n");
        exit(EXIT_FAILURE);
    }
        
    // waiting for thread to finish
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    char *quote = "quote";
    char *music = "music";


    // move file outside 
    for(int i=1;i<=9;i++){
        chdir(music);
        char *file, *num;
        file = (char*)malloc(100); num = (char*)malloc(100);
        sprintf(num,"%d",i);
        strcpy(file,"m");strcat(file,num);strcat(file,".txt");
        //copy and move file to home before encoding
        copy_move(file,home);
        chdir(home);
    }
    for(int i=1;i<=9;i++){
        chdir(quote);
        char *file, *num;
        file = (char*)malloc(100); num = (char*)malloc(100);
        sprintf(num,"%d",i);
        strcpy(file,"q");strcat(file,num);strcat(file,".txt");
        //copy and move file to home before encoding
        copy_move(file,home);
        chdir(home);
    }
    
    if(pthread_create(&tid[0],NULL,&grab_str, (void*)quote)){
        printf("Thread quote error\n");
        exit(EXIT_FAILURE);
    }
    print_cwd();
    // thread 2
    if(pthread_create(&tid[1],NULL,&grab_str, (void*)music)){
        printf("Thread music error\n");
        exit(EXIT_FAILURE);
    }
        
    // waiting for thread to finish
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // buat folder
    make_dir("hasil");
    move_to("hasil","music.txt");
    move_to("hasil","quote.txt");

    char *password = "mihinomenestrainata";
    // zip file hasil
    pid_t idzip = fork();
    if(idzip == 0){
        char *argv[] = {"zip","-P",password, "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip",argv);
    }
    waitpid(idzip,NULL,0);
    
    // hapus folder hasil setelah zip
    pid_t rmdir = fork();
    if(rmdir == 0){
        char *argv[] = {"rm", "-r", "hasil", NULL};
        execv("/bin/rm", argv);
    }
    waitpid(rmdir,NULL,0);


    // thread 1 (buat file txt)
    if(pthread_create(&tid[0],NULL,&create_no_txt, NULL)){
        printf("Thread quote error\n");
        exit(EXIT_FAILURE);
    }
    // thread 2 (unzip file yang tadi lagi, masih ga paham kenapa disuru bolak balik awkwk)
    if(pthread_create(&tid[1],NULL,&unzip_file2, (void*)password)){
        printf("Thread music error\n");
        exit(EXIT_FAILURE);
    }
        
    // waiting for thread to finish
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // move no txt ke folder hasil
    move_to("hasil","no.txt");

    //zip lagi -_-, hapus file zip yang lama dulu
    remove_file("hasil.zip");
    // zip lagi dengan nama yang sama
    idzip = fork();
    if(idzip == 0){
        char *argv[] = {"zip","-P",password, "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip",argv);
    }
    waitpid(idzip,NULL,0);

    // SELESAI

    return 0;
}