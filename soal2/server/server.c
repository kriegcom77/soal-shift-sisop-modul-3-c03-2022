#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080

char *pwd = "/home/ilhamfz/Sisop/Shift3/server";
char *txt_dir = "/home/ilhamfz/Sisop/Shift3/user.txt";
char *database = "/home/ilhamfz/Sisop/Shift3/problem.tsv";

struct problem {
    char title[1001];
    char desc_path[1001];
    char input_path[1001];
    char output_path[1001];
    char path[1001];
};

void fill_folder_problem(char *old_path, char *new_path) {
    FILE *streamOld, *streamNew;
    streamOld = fopen(old_path, "r");
    streamNew = fopen(new_path, "a");

    char temp_data[1024];
    memset(temp_data, 0, sizeof(temp_data));

    while(fgets(temp_data, 1024, streamOld) != NULL) {
        fprintf(streamNew, "%s", temp_data);
    }
    fclose(streamOld);
    fclose(streamNew);
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read( new_socket , buffer, 1024);
    printf("%s\n",buffer );
    send(new_socket , hello , strlen(hello) , 0 );
    printf("Hello message sent\n");
    //create user.txt dir
    FILE* fp_user = fopen(txt_dir, "a");
    fclose(fp_user);
    FILE* fp_database;
    if(fp_database = fopen(database, "r")){
    }else{
        fp_database = fopen(database, "a");
        fprintf(fp_database, "Judul\tAuthor\n");
    }
    fclose(fp_database);
    char command_auth[1], user_logged[101];
    valread = read( new_socket , command_auth, 1024);
    if(command_auth[0]=='r'){
        printf("Username Registered :\n");
        FILE* fp = fopen(txt_dir, "r");
        char line[1024], data_uname[1001][1001];
        int index=0;
        while (fgets(line, sizeof(line), fp)) {
            char *tok = strtok(line, ":");
            printf("%d. %s\n", index+1, tok);
            strcpy(data_uname[index], tok);
            index++;
        }
        fclose(fp);
        char temp[101];
        sprintf(temp, "%d", index);
        send(new_socket, temp, 1024, 0);
        send(new_socket, data_uname, sizeof(data_uname), 0);

        char uname[101], pass[101];
        valread = read(new_socket, uname, 1024);
        valread = read(new_socket, pass, 1024);
        fp = fopen(txt_dir, "a");
        fprintf(fp, "%s:%s\n", uname, pass);
        printf("User '%s' Registered\n", uname);
        fclose(fp);
    }else if(command_auth[0]=='l'){
        FILE* fp = fopen(txt_dir,"r");
        char line[1024], data[1001][1001];
        int index=0;
        printf("Username:Password-List :\n");
        while(fgets(line, sizeof(line), fp)){
            char *tok = strtok(line, "\n");
            printf("%d. %s\n", index+1, tok);
            strcpy(data[index], tok);
            index++;
        }
        fclose(fp);
        char temp[101];
        sprintf(temp, "%d", index);
        send(new_socket, temp, 1024, 0);
        for(int i=0; i<index; i++) send(new_socket, data[i], sizeof(data[i]), 0);
        //send(new_socket, data, sizeof(data), 0);
        valread = read(new_socket, user_logged, 1024);
        printf("user %s already logged in\n", user_logged);
    }
    struct stat st = {0};
    if(stat(pwd, &st) == -1) mkdir(pwd, 0700);
    int flag_logged=0;
    char temp_buff[1024];
    valread = read(new_socket, temp_buff, 1024);
    flag_logged=atoi(temp_buff);
    if(flag_logged=-1){
        while(1){
            int flag_uniq=0;
            char command_user[1001];
            valread = read(new_socket, command_user, 1024);
            if(strcmp(command_user, "add")==0){
                struct problem data;
                while(1){
                    char line[1024], prob[1024];
                    valread = read(new_socket, prob, 1024);
                    FILE* fp = fopen(database, "r");
                    while(fgets(line, sizeof(line), fp)) {
                        char *tok = strtok(line, "\t");
                        if(strcmp(prob, tok) == 0) {
                            send(new_socket, "error", 1024, 0);
                            flag_uniq=1;
                            break;
                        }
                    }
                    fclose(fp);
                    if(flag_uniq==0){
                        send(new_socket, "success", 1024, 0);
                        printf("New problem added by %s\n", user_logged);
                        memset(data.title, 0, sizeof(data.title));
                        strcpy(data.title, prob);
                        break;
                    }
                }
                //initial temp variable
                char temp_desc_path[1001], temp_input_path[1001], temp_output_path[1001];
                valread = read(new_socket, temp_desc_path, 1024);
                valread = read(new_socket, temp_input_path, 1024);
                valread = read(new_socket, temp_output_path, 1024);
                //cek data
                printf("Judul problem: %s\nFilepath description: %s\nFilepath input: %s\nFilepath output: %s\n", data.title, temp_desc_path, temp_input_path, temp_output_path);
                //write to tsv
                FILE* fp = fopen(database, "a");
                fprintf(fp, "%s\t%s\n", data.title, user_logged);
                printf("Problem added to database\n");
                fclose(fp);
                //create path directory problem
                strcpy(data.path, pwd);
                strcat(data.path, "/");
                strcat(data.path, data.title);
                //make folder
                mkdir(data.path, 0700);
                //fill folder problem with description.txt
                strcpy(data.desc_path, data.path);
                strcat(data.desc_path, "/description.txt");
                fill_folder_problem(temp_desc_path, data.desc_path);
                //fill folder problem with input.txt
                strcpy(data.input_path, data.path);
                strcat(data.input_path, "/input.txt");
                fill_folder_problem(temp_input_path, data.input_path);
                //fill folder problem with output.txt
                strcpy(data.output_path, data.path);
                strcat(data.output_path, "/output.txt");
                fill_folder_problem(temp_output_path, data.output_path);
                printf("Problem directory had been made\n");
            }
            else if(strcmp(command_user, "see")==0){
                FILE* fp = fopen(database, "r");
                char temp_data[1024], line[1024];
                while(fgets(line, sizeof(line), fp)){
                    char *title_tok = strtok(line, "\t");
                    char *author_tok = strtok(NULL, "\t");
                    strcpy(temp_data, title_tok);
                    strcat(temp_data, " by ");
                    strcat(temp_data, author_tok);
                    strcat(temp_data, "\n");
                }
                fclose(fp);
                send(new_socket, temp_data, 1024, 0);
            }
            else if(strcmp(command_user, "download")==0){
                struct problem source;
                struct problem destination;
                char title_download[1024];
                valread = read(new_socket, title_download, 1024);
                //client device
                strcpy(destination.path, "/home/ilhamfz/Sisop/Shift3/client/");
                strcat(destination.path, title_download);
                mkdir(destination.path, 0700);
                //make path desc
                strcpy(destination.desc_path, destination.path);
                strcat(destination.desc_path, "/description.txt");
                //make path input
                strcpy(destination.input_path, destination.path);
                strcat(destination.input_path, "/input.txt");
                //server device
                strcpy(source.path, pwd);
                strcat(source.path, "/");
                strcat(source.path, title_download);
                //make path desc source
                strcpy(source.desc_path, source.path);
                strcat(source.desc_path, "/description.txt");
                fill_folder_problem(source.desc_path, destination.desc_path);
                //make path input source
                strcpy(source.input_path, source.path);
                strcat(source.input_path, "/input.txt");
                fill_folder_problem(source.input_path, destination.input_path);
            }
            else if(strcmp(command_user, "submit")==0){
                char title_submit[1024], client_ans_path[1024];
                valread = read(new_socket, title_submit, 1024);
                valread = read(new_socket, client_ans_path, 1024);
                char path_submit[1024];
                strcpy(path_submit, pwd);
                strcat(path_submit, "/");
                strcat(path_submit, title_submit);
                strcat(path_submit, "/output.txt");
                FILE* fp_server = fopen(path_submit, "r");
                FILE* fp_client = fopen(client_ans_path, "r");
                char client_out[1024] = "";
                char server_out[1024] = "";
                char temp_client_data[1024], temp_server_data[1024];
                while(fgets(temp_client_data, 1024, fp_client)) {
                    strcat(client_out, temp_client_data);
                }
                while(fgets(temp_server_data, 1024, fp_server)) {
                    strcat(server_out, temp_server_data);
                }
                if(strcmp(server_out, client_out) == 0) {
                    printf("AC\n");
                    send(new_socket, "AC", 1024, 0);
                }else {
                    printf("WA\n");
                    send(new_socket, "WA", 1024, 0);
                }
                fclose(fp_client);
                fclose(fp_server);
            }
            else if(strcmp(command_user, "logout")==0) break;
        }
    }
    return 0;
}
