#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

char *pwd = "/home/ilhamfz/Sisop/Shift3/client";

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    send(sock , hello , strlen(hello) , 0 );
    printf("Hello message sent\n");
    valread = read( sock , buffer, 1024);
    printf("%s\n", buffer );
    int flag_logged=-1;
    printf("command r to register or l to login\ncommand : ");
    char command_auth[1];
    scanf(" %s", command_auth);
    send(sock, command_auth, 1024, 0);
    if(command_auth[0]=='r'){
        bool flag_num = true, flag_up = true, flag_low = true, flag_uname=false;
        char c, uname[101], pass[101], data_uname[1001][1001];
        valread = read(sock, buffer, 1024);
        int index=atoi(buffer);
        valread = read(sock, data_uname, sizeof(data_uname));
        printf("Username : ");
        while(!flag_uname){
            scanf(" %s", uname);
            for(int i=0; i<index; i++){
                if(strcmp(data_uname[i], uname)==0){
                    flag_uname=true;
                    break;
                }
            }
            if(flag_uname){
                printf("Username already exist\nUsername : ");
                flag_uname=false;
            }else{
                flag_uname=true;
                getchar();
            }
        }
        //pass validation
        while(flag_num || flag_low || flag_up) {
            printf("Password : ");
            scanf(" %s", pass);
            for(int i=0; i<strlen(pass); i++){
                if(pass[i] >= '0' && pass[i] <= '9') flag_num = false;
                if(pass[i] >= 'A' && pass[i] <= 'Z') flag_up = false;
                if(pass[i] >= 'a' && pass[i] <= 'z') flag_low = false;
            }
            if(flag_num || flag_low || flag_up || strlen(pass) < 6){
                printf("\nPassword Incorrect, password must contain the following :\n");
                if (strlen(pass) < 6) printf("- Minimum 6 characters\n");
                if (flag_num) printf("- a number\n");
                if (flag_low) printf("- a lowercase letter\n");
                if (flag_up) printf("- an uppercase letter\n");
                flag_num = flag_up = flag_low = true;
                memset(pass, 0, 101);
            }
        }
        send(sock, uname, 1024, 0);
        send(sock, pass, 1024, 0);
    }else if(command_auth[0]=='l'){
        valread = read(sock, buffer, 1024);
        int index=atoi(buffer);
        char data[1001][1001], data_uname[1001][1001], data_pass[1001][1001];
        for(int i=0; i<index; i++) read(sock, data[i], sizeof(data[i]));
        for(int i=0; i<index; i++){
            char *tok1 = strtok(data[i], ":");
            char *tok2 = strtok(NULL, "\n");
            strcpy(data_uname[i], tok1);
            strcpy(data_pass[i], tok2);
            //printf("Cek aja\n%s:%s\n", data_uname[i], data_pass[i]);
        }
        bool flag=false;
        char uname[1001], pass[1001];
        while(!flag){
            int flag_uname=-1;
            while(flag_uname==-1){
                printf("Username : ");
                scanf(" %s", uname);
                for(int i=0; i<index; i++){
                    flag_uname=-2;
                    if(strcmp(data_uname[i], uname)==0){
                        flag_uname=i;
                        break;
                    }
                }
                //printf("This username is not registered\n");
            }
            printf("Password : ");
            scanf(" %s", pass);
            if(flag_uname==-2){
                printf("Your username or password is wrong\n");
                continue;
            }
            if(strcmp(data_pass[flag_uname], pass)==0){
                flag=true;
                flag_logged=flag_uname;
                printf("Welcome %s, nice to meet u :D\n", data_uname[flag_uname]);
                send(sock, data_uname[flag_uname], 1024, 0);
            }else printf("Your username or password is wrong\n");
            //printf("Your password is '%s'\n", data_pass[flag_uname]);
        }
    }
    struct stat st = {0};
    if(stat(pwd, &st) == -1) mkdir(pwd, 0700);
    char temp[101];
    sprintf(temp, "%d", flag_logged);
    send(sock, temp, 1024, 0);
    if(flag_logged!=-1){
        while(1){
            printf("Available commands :\n - add\n - see\n - download <problem_name>\n - submit <problem_name> <your_output.txt_path>\n - logout\n");
            char command_user[1001];
            printf("Input your command : ");
            scanf(" %s", command_user);
            send(sock, command_user, 1024, 0);
            if(strcmp(command_user, "add")==0){
                while(1){
                    char prob[1024];
                    char add_status[101];
                    printf("Enter your new problem title : ");
                    scanf(" %s", prob);
                    send(sock, prob, 1024, 0);
                    valread = read(sock, add_status, 1024);
                    if(strcmp(add_status, "error")==0){
                        printf("Problem title have been used, please try another title!\n");
                    }else{
                        printf("Succes to add problem\n");
                        break;
                    }
                }
                char desc_path[1024], input_path[1024], output_path[1024];
                // add description
                printf("Enter description file path : ");
                scanf(" %s", desc_path);
                // add input
                printf("Enter input file path : ");
                scanf(" %s", input_path);
                // add output
                printf("Enter output file path : ");
                scanf(" %s", output_path);

                send(sock, desc_path, 1024, 0);
                send(sock, input_path, 1024, 0);
                send(sock, output_path, 1024, 0);
                printf("Problem Uploaded to the server!\n\n");
            }
            else if(strcmp(command_user, "see")==0){
                char temp_print[1024];
                valread = read(sock, temp_print, 1024);
                printf("Problem List :\n%s", temp_print);
            }
            else if(strcmp(command_user, "download")==0){
                char command_next[1024], folder_path_temp[1024], desc_path_temp[1024], input_path_temp[1024];;
                scanf("%s", command_next);
                send(sock, command_next, 1024, 0);
                printf("Complete to download problem\n");
            }
            else if(strcmp(command_user, "submit")==0){
                char title_submit[1024], ans_path[1024];
                scanf(" %s %s", title_submit, ans_path);
                send(sock, title_submit, 1024, 0);
                send(sock, ans_path, 1024, 0);
                char status_submit[1024];
                valread = read(sock, status_submit, 1024);
                if(strcmp(status_submit, "AC")==0) printf("AC\n");
                else printf("WA\n");
            }
            else if(strcmp(command_user, "logout")==0){
                break;
            }
            else printf("Wrong command\n");
        }
    }
    return 0;
}
