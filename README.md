# soal-shift-sisop-modul-3-C03-2022



## Anggota Kelompok

```
Nama    : Ida Bagus Kade Rainata Putra Wibawa
Nrp     : 5025201235

Nama    : Moh. Ilham Fakhri Zamzami
Nrp     : 5025201275

Nama    : Ichsanul Aulia
Nrp     : 05111840007001
```

Soal 1

Kendala:
- Pehaman baru tentang thread

1.a Download 2 file zip, quote dan music, lalu extract secara bersamaan. Proses extract dapat dilakukan dengan thread. Berikut codenya

**Fungsi download**
``` c
// inisialisasi
void initialize(){
    char *output[] = {"quote.zip", "music.zip"};
    char *links[] = {
        "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
        "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
    
    // make a folder
    make_dir("quote");
    make_dir("music");

    // download
    for(int i=0; i<2; i++){
        pid_t child = fork();
        if(child == 0){
            char *argv[] = {"wget","-q","--no-check-certificate",links[i],"-O",output[i],NULL};
            execv("/usr/local/bin/wget",argv);
        }
        waitpid(child, NULL, 0);
    }

    sleep(1);
}
```
**Fungsi unzip**
```c
// unzip_file
void *unzip_file(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){
        child = fork();
        if(child == 0){
            char *argv[] = {"unzip","-qq","quote.zip","-d","quote",NULL};
            execv("/usr/bin/unzip",argv);
        }
        waitpid(child,NULL,0);
    }
    else if(pthread_equal(id, tid[1])){
        child = fork();
        if(child == 0){
            char *argv[] = {"unzip","-qq","music.zip","-d","music",NULL};
            execv("/usr/bin/unzip",argv);
        }
        waitpid(child,NULL,0);
    }

    return NULL;
}
```
**Thread pada fungsi main**
```c
 initialize();
    print_cwd();
    if(pthread_create(&tid[0],NULL,&unzip_file, NULL)){
        printf("Thread quote error\n");
        exit(EXIT_FAILURE);
    }
    print_cwd();
    // thread 2
    if(pthread_create(&tid[1],NULL,&unzip_file, NULL)){
        printf("Thread music error\n");
        exit(EXIT_FAILURE);
    }
        
    // waiting for thread to finish
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
```

1.b Decode hasil file txt tadi dengan base64 kemudian hasilnya disatukan dan diberi nama quote.txt dan music.txt. Proses ini dilakukan dengan execv base64

**Fungsi decode**
```c
// grab str quote
void *grab_str(void *dire){
    char *dir;
    dir = (char *)dire;
    //printf("%s\n", dir);
    //chdir(dir);
    char *zipfile,*awal;
    if(strcmp(dir,"quote") == 0){
        awal = "q"; 
    }
    else{
        awal = "m";
    }
    char *txt;
    char *num;
    char *file_name;
    char txt_merge[100];
    char final_txt[100];

    for(int i=1;i<=9;i++){
        txt = (char*)malloc(100); num = (char*)malloc(100); file_name = (char*)malloc(100);
        strcpy(file_name, awal);
        sprintf(num,"%d",i); strcat(file_name,num);
        strcpy(txt_merge,file_name); strcat(txt_merge,"_new.txt");
        strcat(file_name,".txt");

        pid_t id = fork();
        if(id == 0){
            char *argv[] = {"base64", "-d", "-i", file_name, "-o", txt_merge,NULL};
            execv("/usr/bin/base64",argv);
        }
        waitpid(id,NULL,0);
        remove_file(file_name);

        //joining the decoded file
        char *temp = (char*)malloc(100);
        strcpy(final_txt,dir);strcat(final_txt,".txt");
        FILE *file = fopen(final_txt,"a");
        FILE *input = fopen(txt_merge,"r");
        fgets(temp,100,input);
        fputs(temp,file);
        fprintf(file,"\n");
        fclose(file);fclose(input);
        remove_file(txt_merge);


    }
    chdir(home);
    return NULL;
}
```
Sebelum itu, saya pindahkan file seluruh file ke main directory karena akan dilakuakn decode disana

**Memindahkan file**
```c
char *quote = "quote";
    char *music = "music";


    // move file outside 
    for(int i=1;i<=9;i++){
        chdir(music);
        char *file, *num;
        file = (char*)malloc(100); num = (char*)malloc(100);
        sprintf(num,"%d",i);
        strcpy(file,"m");strcat(file,num);strcat(file,".txt");
        //copy and move file to home before encoding
        copy_move(file,home);
        chdir(home);
    }
    for(int i=1;i<=9;i++){
        chdir(quote);
        char *file, *num;
        file = (char*)malloc(100); num = (char*)malloc(100);
        sprintf(num,"%d",i);
        strcpy(file,"q");strcat(file,num);strcat(file,".txt");
        //copy and move file to home before encoding
        copy_move(file,home);
        chdir(home);
    }
```
**Thread pada main**
```c
if(pthread_create(&tid[0],NULL,&grab_str, (void*)quote)){
        printf("Thread quote error\n");
        exit(EXIT_FAILURE);
    }
    print_cwd();
    // thread 2
    if(pthread_create(&tid[1],NULL,&grab_str, (void*)music)){
        printf("Thread music error\n");
        exit(EXIT_FAILURE);
    }
        
    // waiting for thread to finish
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
```

1.c Pindahkan hasil decode ke folder hasil. Cukup membuat folder "hasil" dan pindahkan filenya

**Statements pada main**
``` c
 // buat folder
    make_dir("hasil");
    move_to("hasil","music.txt");
    move_to("hasil","quote.txt");
```

1.d Folder hasil dizip dengan nama hasil.zip dengan password 'mihinomenest' + user (rainata). Zip digunakan execv

**Statements di main**
```c
char *password = "mihinomenestrainata";
    // zip file hasil
    pid_t idzip = fork();
    if(idzip == 0){
        char *argv[] = {"zip","-P",password, "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip",argv);
    }
    waitpid(idzip,NULL,0);
```

1.e Unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan . Lalu zip kembali kedua file hasil dengan nama hasil.zip serta dengan password yang sama. Proses ini dapat diselesaikan dengan thread.

**Fungsi unzip ke 2**
```c
// unzip file 2
void * unzip_file2(void *arg){
    char *pass = (char*)arg;
    pid_t id = fork();
    if(id == 0){
        char *argv[] = {"unzip","-P",pass, "hasil.zip",NULL};
        execv("/usr/bin/unzip",argv);
    }
    waitpid(id,NULL,0);
    return NULL;
}
```
**Fungsi membuat no.txt**
```c
// create no txt
void *create_no_txt(void *arg){
    FILE *file = fopen("no.txt", "w");
    fputs("No",file);
    fclose(file);
    return NULL;
}
```
**Fungsi thread di main serta fungsi zip**
```c
// thread 1 (buat file txt)
    if(pthread_create(&tid[0],NULL,&create_no_txt, NULL)){
        printf("Thread quote error\n");
        exit(EXIT_FAILURE);
    }
    // thread 2 (unzip file yang tadi lagi, masih ga paham kenapa disuru bolak balik awkwk)
    if(pthread_create(&tid[1],NULL,&unzip_file2, (void*)password)){
        printf("Thread music error\n");
        exit(EXIT_FAILURE);
    }
        
    // waiting for thread to finish
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // move no txt ke folder hasil
    move_to("hasil","no.txt");

    //zip lagi -_-, hapus file zip yang lama dulu
    remove_file("hasil.zip");
    // zip lagi dengan nama yang sama
    idzip = fork();
    if(idzip == 0){
        char *argv[] = {"zip","-P",password, "-r", "hasil.zip", "hasil", NULL};
        execv("/usr/bin/zip",argv);
    }
    waitpid(idzip,NULL,0);

    // SELESAI
```

## Nomer 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

inisiasi working space kedalam beberapa variebel untuk mempermudah dalam menulisakan code
pada client.c :
```
char *pwd = "/home/ilhamfz/Sisop/Shift3/client";
```
pada server.c :
```
char *pwd = "/home/ilhamfz/Sisop/Shift3/server";
char *txt_dir = "/home/ilhamfz/Sisop/Shift3/user.txt";
char *database = "/home/ilhamfz/Sisop/Shift3/problem.tsv";
```

### 2A
Memuat fungsi login dan regist <br>
Diberikan pilihan untuk r sebagai regist dan l sebagai login
1. dalam regist memiliki alur yaitu, pertama ambil data dari user.txt dan catat pada array of char data_uname. Lalu, bedakan inputan dengan masing" data array of char tersebut, jika uname uniq makan lanjut pada password, saat pasword cek juga untuk minimal 6 character, uppercase character, lowercase character, dan number character pada pasword tersebut. 
2. dalam login alurnya yaitu pertama buka user txt dan cek apakah username dan pass telah terdaftar, jika iya maka lanjut, jika tidak maka ada printf `your username or pasword is wrong!`, dan akan loop hingga inputan user dan pass dinyatakan benar atau terdaftar.
<br>
berikut sourecode pada client :

```
...
    int flag_logged=-1;
    printf("command r to register or l to login\ncommand : ");
    char command_auth[1];
    scanf(" %s", command_auth);
    send(sock, command_auth, 1024, 0);
    if(command_auth[0]=='r'){
        bool flag_num = true, flag_up = true, flag_low = true, flag_uname=false;
        char c, uname[101], pass[101], data_uname[1001][1001];
        valread = read(sock, buffer, 1024);
        int index=atoi(buffer);
        valread = read(sock, data_uname, sizeof(data_uname));
        printf("Username : ");
        while(!flag_uname){
            scanf(" %s", uname);
            for(int i=0; i<index; i++){
                if(strcmp(data_uname[i], uname)==0){
                    flag_uname=true;
                    break;
                }
            }
            if(flag_uname){
                printf("Username already exist\nUsername : ");
                flag_uname=false;
            }else{
                flag_uname=true;
                getchar();
            }
        }
        //pass validation
        while(flag_num || flag_low || flag_up) {
            printf("Password : ");
            scanf(" %s", pass);
            for(int i=0; i<strlen(pass); i++){
                if(pass[i] >= '0' && pass[i] <= '9') flag_num = false;
                if(pass[i] >= 'A' && pass[i] <= 'Z') flag_up = false;
                if(pass[i] >= 'a' && pass[i] <= 'z') flag_low = false;
            }
            if(flag_num || flag_low || flag_up || strlen(pass) < 6){
                printf("\nPassword Incorrect, password must contain the following :\n");
                if (strlen(pass) < 6) printf("- Minimum 6 characters\n");
                if (flag_num) printf("- a number\n");
                if (flag_low) printf("- a lowercase letter\n");
                if (flag_up) printf("- an uppercase letter\n");
                flag_num = flag_up = flag_low = true;
                memset(pass, 0, 101);
            }
        }
        send(sock, uname, 1024, 0);
        send(sock, pass, 1024, 0);
    }else if(command_auth[0]=='l'){
        valread = read(sock, buffer, 1024);
        int index=atoi(buffer);
        char data[1001][1001], data_uname[1001][1001], data_pass[1001][1001];
        for(int i=0; i<index; i++) read(sock, data[i], sizeof(data[i]));
        for(int i=0; i<index; i++){
            char *tok1 = strtok(data[i], ":");
            char *tok2 = strtok(NULL, "\n");
            strcpy(data_uname[i], tok1);
            strcpy(data_pass[i], tok2);
            //printf("Cek aja\n%s:%s\n", data_uname[i], data_pass[i]);
        }
        bool flag=false;
        char uname[1001], pass[1001];
        while(!flag){
            int flag_uname=-1;
            while(flag_uname==-1){
                printf("Username : ");
                scanf(" %s", uname);
                for(int i=0; i<index; i++){
                    flag_uname=-2;
                    if(strcmp(data_uname[i], uname)==0){
                        flag_uname=i;
                        break;
                    }
                }
                //printf("This username is not registered\n");
            }
            printf("Password : ");
            scanf(" %s", pass);
            if(flag_uname==-2){
                printf("Your username or password is wrong\n");
                continue;
            }
            if(strcmp(data_pass[flag_uname], pass)==0){
                flag=true;
                flag_logged=flag_uname;
                printf("Welcome %s, nice to meet u :D\n", data_uname[flag_uname]);
                send(sock, data_uname[flag_uname], 1024, 0);
            }else printf("Your username or password is wrong\n");
            //printf("Your password is '%s'\n", data_pass[flag_uname]);
        }
    }
...
```
dan berikut adalah sourecode pada server :

```
...
    char command_auth[1], user_logged[101];
    valread = read( new_socket , command_auth, 1024);
    if(command_auth[0]=='r'){
        printf("Username Registered :\n");
        FILE* fp = fopen(txt_dir, "r");
        char line[1024], data_uname[1001][1001];
        int index=0;
        while (fgets(line, sizeof(line), fp)) {
            char *tok = strtok(line, ":");
            printf("%d. %s\n", index+1, tok);
            strcpy(data_uname[index], tok);
            index++;
        }
        fclose(fp);
        char temp[101];
        sprintf(temp, "%d", index);
        send(new_socket, temp, 1024, 0);
        send(new_socket, data_uname, sizeof(data_uname), 0);

        char uname[101], pass[101];
        valread = read(new_socket, uname, 1024);
        valread = read(new_socket, pass, 1024);
        fp = fopen(txt_dir, "a");
        fprintf(fp, "%s:%s\n", uname, pass);
        printf("User '%s' Registered\n", uname);
        fclose(fp);
    }else if(command_auth[0]=='l'){
        FILE* fp = fopen(txt_dir,"r");
        char line[1024], data[1001][1001];
        int index=0;
        printf("Username:Password-List :\n");
        while(fgets(line, sizeof(line), fp)){
            char *tok = strtok(line, "\n");
            printf("%d. %s\n", index+1, tok);
            strcpy(data[index], tok);
            index++;
        }
        fclose(fp);
        char temp[101];
        sprintf(temp, "%d", index);
        send(new_socket, temp, 1024, 0);
        for(int i=0; i<index; i++) send(new_socket, data[i], sizeof(data[i]), 0);
        //send(new_socket, data, sizeof(data), 0);
        valread = read(new_socket, user_logged, 1024);
        printf("user %s already logged in\n", user_logged);
    }
...
```

### bawahnya ini blm edit
### 2B
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
```
...
awk '
END {print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "request per jam"}' $loclog>$locpwd/ratarata.txt
...
```
Gunakan `NR` untuk mengetahui jumlah line yang ada dalam file log. `(NR-1)/12` kurangkan 1 karena pada line pertama file log bukan merupakan request, lalu dibagi 12 untuk menghitung rata-rata per jamnya karena dalam file log hanya ditampilkan dari jam 00.00 hingga jam 12.00.
Hasil tersebut lalu di print kedalam file `ratarata.txt` dalam working space `$locpwd`


### 2C
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak
melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
```
awk -F\" ' {print $2}' $loclog | sort | uniq -c | sort -rn | head -n1 | awk '
{print "IP yang paling banyakmengakses server adalah:", $2, "sebanyak", $1, "request"}' >$locpwd/result.txt
```
Dalam code tersebut digunakan dua awk sekaligus. Awk pertama digunakan untuk menampilkan seluruh ip dengan menghilangkan ip yang duplicate dengan syntax `uniq` dan dihitung masing-masing ip tersebut oleh syntax `-c` dan di sorting dengan urutan jumlah ip tersebut, namun hanya ditampilkan baris pertama dari daftar ip dan jumlahnya untuk mendapatkan ip yang melakukan request paling banyak. Awk kedua digunakan untuk memisahkan data ip dan data jumlah ip request dari ip tersebut, lalu dilakukannya print kedalam file `result.txt` pada working space `$locpwd`


### 2D
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
```
awk -F\" ' /curl/ {++n} END {print "Ada", n, "request yang menggunakan curl sebagai user-agent"}' $loclog>>$locpwd/result.txt
```
Dalam awk tersebut, digunakan pencarian kata `curl` dalam setiap line data log, dan dicatata jumlahnya dalam variabel `n`, lalu dilakukannya print kedalam file `result.txt` pada working space `$locpwd`


### 2E
Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
```
awk -F\" ' /2022:02/ {print $2}' $loclog | uniq >> $locpwd/result.txt
```
Digunakan pula awk untuk menampilkan data dari file log yang memiliki kecocokan dengan `2022:02` dan tampilkan setiap ip dari data yang telah diperoleh. Gunakan syntax `uniq` untuk menghilangkan data ip yang duplicate, lalu dilakukannya print kedalam file `result.txt` pada working space `$locpwd`


### 2 Hasil
![Nomer_2_Hasil](/uploads/93f466df07eb35845cd4ff4c130be4dc/Nomer_2_Hasil.png)

Setelah file `soal2_forensic_dapos.sh` dijalankan, ditemukannya folder `forensic_log_website_daffainfo_log` dalam direktory `$locfolder`. Dalam folder tersebut juga ditemukan file `ratarata.txt` dan `result.txt` berisikan data seperti gambar diatas.
